package eu.inmite.android.lib.zbar_scanner;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.FrameLayout;
import com.actionbarsherlock.app.SherlockActivity;
import eu.inmite.android.lib.R;
import net.sourceforge.zbar.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Tomáš Kypta
 * @since 15/11/2013
 */
@TargetApi(Build.VERSION_CODES.ECLAIR)
public class ZBarCaptureActivity extends SherlockActivity {

	private static final int MIN_PREVIEW_PIXELS = 470 * 320; // normal screen
	private static final int MAX_PREVIEW_PIXELS = 1280 * 720;
	private static final String TAG = "CaptureActivity";
	private int mCameraId;

	private Camera mCamera;
	private CameraPreview mPreview;
	private Handler mAutoFocusHandler;
	private BeepManager mBeepManager;

	private ImageScanner mScanner;

	private boolean mIsPreviewing = true;

	static {
		System.loadLibrary("iconv");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.zbar_capture);

		mBeepManager = new BeepManager(this);
		mAutoFocusHandler = new Handler();
		mCamera = getCameraInstance();

		if (mCamera != null) {
			setupCamera(mCamera);

			/* Instance barcode scanner */
			mScanner = new ImageScanner();
			mScanner.setConfig(0, Config.X_DENSITY, 3);
			mScanner.setConfig(0, Config.Y_DENSITY, 3);

			mPreview = new CameraPreview(this, mCamera,mCameraId, previewCb, autoFocusCB);
			FrameLayout preview = (FrameLayout) findViewById(R.id.zbarCameraPreview);
			preview.addView(mPreview);
		} else {
			onCameraFailed();
		}
	}

	protected void setupCamera(Camera camera) {
		final Camera.Parameters parameters = camera.getParameters();
		final Point screenResolution = new Point(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels);
		final Point previewSize = findBestPreviewSizeValue(parameters, screenResolution);

		parameters.setPreviewSize(previewSize.x, previewSize.y);
		parameters.setPreviewFormat(PixelFormat.YCbCr_420_SP);
		camera.setParameters(parameters);
	}

	public void onPause() {
		super.onPause();
		releaseCamera();
	}

	protected void onCameraFailed() {
		finish();
	}

	protected void releaseCamera() {
		if (mCamera != null) {
			mIsPreviewing = false;
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

	/** A safe way to get an instance of the Camera object. */
	public Camera getCameraInstance(){
		Camera c = null;
		int numberOfCameras = Camera.getNumberOfCameras();
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		for (int i = 0; i < numberOfCameras; i++) {
			Camera.getCameraInfo(i, cameraInfo);
			if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
				try {
					c = Camera.open(i);
					mCameraId = i;
				} catch (Exception e){
				}
			}
		}

		return c;
	}


	// Mimic continuous auto-focusing
	private Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback() {
		public void onAutoFocus(boolean success, Camera camera) {
			mAutoFocusHandler.postDelayed(doAutoFocus, 1000);
		}
	};

	private Runnable doAutoFocus = new Runnable() {
		public void run() {
			if (mIsPreviewing)
				mCamera.autoFocus(autoFocusCB);
		}
	};

	private Camera.PreviewCallback previewCb = new Camera.PreviewCallback() {
		public void onPreviewFrame(byte[] data, Camera camera) {
			Camera.Parameters parameters = camera.getParameters();
			Camera.Size size = parameters.getPreviewSize();

			Image barcode = new Image(size.width, size.height, "Y800");
			barcode.setData(data);

			int result = mScanner.scanImage(barcode);

			if (result != 0) {
				mIsPreviewing = false;
				mCamera.setPreviewCallback(null);
				mCamera.stopPreview();

				onScanFinished();

				SymbolSet syms = mScanner.getResults();
				for (Symbol sym : syms) {
					if (Symbol.QRCODE == sym.getType()) {
						Intent intent = new Intent();
						intent.putExtra("SCAN_RESULT", sym.getData());
						setResult(RESULT_OK, intent);
						finish();
					}
				}
			}
		}
	};

	protected void onScanFinished() {
		mBeepManager.playBeepSoundAndVibrate();
	}

	protected Point findBestPreviewSizeValue(Camera.Parameters parameters, Point screenResolution) {
		List<Camera.Size> rawSupportedSizes = parameters.getSupportedPreviewSizes();
		if (rawSupportedSizes == null) {
			Log.w(TAG, "Device returned no supported preview sizes; using default");
			Camera.Size defaultSize = parameters.getPreviewSize();
			return new Point(defaultSize.width, defaultSize.height);
		}

		// Sort by size, descending
		List<Camera.Size> supportedPreviewSizes = new ArrayList<Camera.Size>(rawSupportedSizes);
		Collections.sort(supportedPreviewSizes, new Comparator<Camera.Size>() {
			@Override
			public int compare(Camera.Size a, Camera.Size b) {
				int aPixels = a.height * a.width;
				int bPixels = b.height * b.width;
				if (bPixels < aPixels) {
					return -1;
				}
				if (bPixels > aPixels) {
					return 1;
				}
				return 0;
			}
		});

		if (Log.isLoggable(TAG, Log.INFO)) {
			StringBuilder previewSizesString = new StringBuilder();
			for (Camera.Size supportedPreviewSize : supportedPreviewSizes) {
				previewSizesString.append(supportedPreviewSize.width).append('x')
						.append(supportedPreviewSize.height).append(' ');
			}
			Log.i(TAG, "Supported preview sizes: " + previewSizesString);
		}

		Point bestSize = null;

		double diff = Double.POSITIVE_INFINITY;
		for (Camera.Size supportedPreviewSize : supportedPreviewSizes) {
			int realWidth = supportedPreviewSize.width;
			int realHeight = supportedPreviewSize.height;
			int pixels = realWidth * realHeight;
			if (pixels < MIN_PREVIEW_PIXELS || pixels > MAX_PREVIEW_PIXELS) {
				continue;
			}

			double screenRatio = screenResolution.x / (double) screenResolution.y;
			if (screenRatio < 1) {
				screenRatio = 1/screenRatio;
			}

			double r = realWidth / (double) realHeight;
			if (r < 1) {
				r = 1/r;
			}

			if (r / screenRatio >= 1) {
				if (r / screenRatio < diff) {
					diff = r / screenRatio;
					bestSize = new Point(realWidth, realHeight);
				}
			} else {
				if (screenRatio / r < diff) {
					diff = screenRatio / r;
					bestSize = new Point(realWidth, realHeight);
				}
			}
		}

		if (bestSize == null) {
			Camera.Size defaultSize = parameters.getPreviewSize();
			bestSize = new Point(defaultSize.width, defaultSize.height);
			Log.i(TAG, "No suitable preview sizes, using default: " + bestSize);
		}

		Log.i(TAG, "CameraConfigurationManager.findBestPreviewSizeValue() - found best approximate preview size: " + bestSize);
		return bestSize;
	}

}
